

earthlingCalendar.prototype.checkDate = function (date)
{
	if (!(date instanceof Date))
	{
		console.log("Проверяемое значение не является объектом Date:" ,date);
		console.trace();
		return false;
	}

	if (isNaN(date.valueOf()))
	{
		console.log("Проверяемое значение является объектом Date, но содержит не верное значение:" ,date);
		console.trace();
		return false;
	}

	return true;
};

/**
 * http://stackoverflow.com/questions/2706125/javascript-function-to-add-x-months-to-a-date#answer-29399462
 */
earthlingCalendar.prototype.getDateWithAddMonthToDate = function (date ,value)
{
	this.checkDate(date);

	date = new Date(date.valueOf());
	var n = date.getDate();
	date.setDate(1);
	date.setMonth(date.getMonth() + value);
	date.setDate(Math.min(n, this.getDaysInMonth(date.getFullYear() ,date.getMonth())));

	return date;
};


earthlingCalendar.prototype.isLeapYear = function (year) {
	return (((year % 4 === 0) && (year % 100 !== 0)) || (year % 400 === 0));
};

earthlingCalendar.prototype.getDaysInMonth = function (year, month) {
	return [31, (this.isLeapYear(year) ? 29 : 28), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][month];
};

// Функция выбора даты для интервала:
earthlingCalendar.prototype.onClickOnToday = function()
{
	var calendar = this.calendar;
	var date = new Date();
	calendar.setTimeByServerTimeDiff(date);
	switch (true) {
		case (date.getDate()     != calendar.date.getDate()):
		case (date.getMonth()    != calendar.date.getMonth()):
		case (date.getFullYear() != calendar.date.getFullYear()):
			calendar.date = date;
			calendar.print();
			break;
	}
};

// Функция выбора даты для интервала:
earthlingCalendar.prototype.daySelect = function()
{
	var calendar = this.calendar;

// Если есть выбранная дата ... :
	if (calendar.DayTD != null)
	{
		calendar.markDaysAndGetEvents(calendar.DayTD ,this);

		//calendar.containers.eventsCount.innerHTML = calendar.wordFormEventsCount(calendar.getEvents(calendar.DayTD ,this).length);

		// Вывести интервал:
		calendar.printInterval(this);

		// Вывести список мероприятий:
		if (calendar.onIntervalSelected)
			calendar.onIntervalSelected(
				 calendar
				,calendar.interval.start
				,calendar.interval.end
				,calendar.interval.startString
				,calendar.interval.endString
			);

		// Обнулить хранение даты:
		calendar.DayTD = null;
	}
	else {
		// Сбросить отметки о выборе:
		for (var day = 0 ; day < calendar.DAYs.length ; day++)
		{
			calendar.DAYs[day].className = calendar.DAYs[day].className.replace(new RegExp(" ?" + calendar.selectedDayClassName ,"mig") ,"");
		}

		// Сбросить запись интервала:
		calendar.containers.interval.innerHTML = "";

		// Отметить выбранную дату:
		this.className = this.className + " " + calendar.selectedDayClassName;

		// И запомнить:
		calendar.DayTD = this;

		// Вывести интервал:
		calendar.printInterval(this);
		calendar.containers.interval.innerHTML += " – выберите вторую дату";
	}

};

// Функция выбора даты для интервала:
earthlingCalendar.prototype.markDaysAndGetEvents = function(tdFirst ,tdLast)
{
	// Индекс даты в массиве:
	var firstDayINDEX = this.DAYs.indexOf(tdFirst);
	var nextDayINDEX  = this.DAYs.indexOf(tdLast);
	if (firstDayINDEX > nextDayINDEX) {
		firstDayINDEX = this.DAYs.indexOf(tdLast);
		nextDayINDEX  = this.DAYs.indexOf(tdFirst);
	}

	// Пока индексы не совпадут ... :
	var eventsCount = [];
	while (firstDayINDEX <= nextDayINDEX) {
		this.DAYs[firstDayINDEX].className = this.DAYs[firstDayINDEX].className + " " + this.selectedDayClassName;

		if (this.DAYs[firstDayINDEX].events)
			eventsCount = eventsCount.concat(this.DAYs[firstDayINDEX].events);

		firstDayINDEX++;
	}

	return eventsCount;
};





// Функция отображения интервала:
earthlingCalendar.prototype.firstAndSecond = function(FirstDAY ,SecondDAY){
	this.checkDate(FirstDAY);
	this.checkDate(SecondDAY);

	if (FirstDAY.valueOf() < SecondDAY.valueOf())
		return [FirstDAY, SecondDAY];
	else return [SecondDAY, FirstDAY];
};


// Функция отображения интервала:
earthlingCalendar.prototype.printInterval = function(DayTD)
{
// Функция получения записи интервала дат:
	var day = function(FirstDAY, SecondDAY)
		{
			return ""
				+ ( SecondDAY ? "с " : "" )
				+ FirstDAY.date
				+ ( SecondDAY ? (" по " + SecondDAY.date) : "" )
				+ " "
				+ this.MONTH[1][FirstDAY.month]
				+ " ";
		};

// Функция получения года интервала дат:
	var year = function( date )
		{
			if ( date.year != (new Date()).getFullYear() )
				return (date.year + " года");
			else return "";
		};

	this.settingInterval = true;

	var ByDATE = [this.DayTD, DayTD];
	if (this.DayTD.timestamp > DayTD.timestamp)
		ByDATE = [DayTD ,this.DayTD];

	//this.removeEventsOnHashChange();

	var fromDate = this.date.getFullYear() + "-" + (this.date.getMonth() + 1) + "-" + this.date.getDate();

	var start = ByDATE[0].year + "-" + (ByDATE[0].month + 1) + "-" + ByDATE[0].date;
	var end   = ByDATE[1].year + "-" + (ByDATE[1].month + 1) + "-" + ByDATE[1].date;


	var reg = /start=[^&]*/;
	var value = "start=" + fromDate;
	if (reg.test(location.hash))
		location.hash = location.hash
			.replace(reg ,value);
	else
		location.hash = (location.hash ? (location.hash + "&") : "#") + value;

	reg = /range=[^&]*/;
	value = "range=" + start + "/" + end;
	if (reg.test(location.hash))
		location.hash = location.hash
			.replace(reg ,value);
	else
		location.hash = (location.hash ? (location.hash + "&") : "#") + value;


	this.interval = {
		 start       : ByDATE[0].dateObj
		,end         : ByDATE[1].dateObj
		,startTD     : ByDATE[0]
		,endTD       : ByDATE[1]
		,startString : start
		,endString   : end
	};

	Object.freeze(this.interval);

	//this.setEventsOnHashChange();
	this.settingInterval = false;

	var interval = this.containers.interval;

// Если вобрана одна дата ... :
	if ( this.DayTD == DayTD )
		{
		// Вывести интервал дат:
			interval.innerHTML = ""
				+ day.call(this ,this.DayTD) + year.call(this ,this.DayTD);
		}
// Если даты НЕ равны ... :
	else
		{
		// Если месяц первой даты равен месяцу второй даты ... :
			if ( this.DayTD.month == DayTD.month )
				{
				// Вывести интервал дат:
					interval.innerHTML = ""
						+ day.call(this ,ByDATE[0], ByDATE[1]) + year.call(this ,ByDATE[0]);
				}
		// Если месяцы НЕ равны ... :
			else
				{
				// Если года равны ... :
					if ( this.DayTD.year == DayTD.year )
						{
						// Вывести интервал дат:
							interval.innerHTML = "с "
								+ day.call(this ,ByDATE[0])
								+ " по "
								+ day.call(this ,ByDATE[1]) + year.call(this ,ByDATE[0]);
						}
					else
						{
						// Вывести интервал дат:
							interval.innerHTML = "с "
								+ day.call(this ,ByDATE[0]) + ByDATE[0].year + " года"
								+ " по "
								+ day.call(this ,ByDATE[1]) + ByDATE[1].year + " года";
						}
				}
		}

	if (this.DayTD && DayTD)
		interval.innerHTML = interval.innerHTML + " – " + this.wordFormEventsCount(this.markDaysAndGetEvents(ByDATE[0] ,ByDATE[1]).length)

};





// Функция выбора месяца:
earthlingCalendar.prototype.previousOrNextMonth = function()
{
	var calendar = this.calendar;

	var Month = this.parentNode;
// Перейти к ячейке:
	while ( Month.tagName != "TD" )
		Month = Month.parentNode;

	var TO = 1;
	if (this.date > 10)
		TO = -1;

// Если НЕТ предыдущей или следующей ячейки ... :
	if (
			( TO == -1 && Month.previousElementSibling == undefined )
				||
			( TO == 1 && Month.nextElementSibling == undefined )
		)
		{
		// ... то пересобрать календарь перейдя к предыдущему или следующему месяцу:
			calendar.date = calendar.getDateWithAddMonthToDate(calendar.date ,TO);

		// Пере-собрать календарь и выбрать переданную дату:
			calendar.print(this.date, this.month ,this.year);

			return;
		}

	var DayINDEX = 0;
	while (
			this.date != calendar.DAYs[DayINDEX].date
				||
			this.month != calendar.DAYs[DayINDEX].month
		)
		{
			DayINDEX++;
			if ( calendar.DAYs[DayINDEX] == undefined )
				break;
		}
// Дата интервала:
	calendar.DAYs[DayINDEX].click();
};


earthlingCalendar.prototype.setTimeByServerTimeDiff = function (date)
{
	this.checkDate(date);

	var localDiff = -((new Date()).getTimezoneOffset() * 60);
	if (this.serverTimeZoneDiff != localDiff)
	{
		var diff = this.serverTimeZoneDiff - localDiff;
		date.setSeconds(date.getSeconds() + diff);
	}

	return date;
};




// Функция выбора месяца:
earthlingCalendar.prototype.selectMonth = function(event)
{
// Для IE:
	event = event || window.event;

// Координаты нажатия мыши:
	var start = event.clientY;

	var calendar = this.calendar;

	calendar.MonthTD = this;

	calendar.selectingMonth = 0;

	window.onmousemove = function(event)
		{
			var diff = start - event.clientY;

			var NewMONTH = Math.round(diff / (window.innerHeight * 0.05));

			if (NewMONTH)
				calendar.selectingMonth = NewMONTH;

		// Новая дата из основной для изменений:
			calendar.NewDATE = calendar.getDateWithAddMonthToDate(calendar.MonthTD.dateObj ,NewMONTH);

		// Изменить месяц в ячейке:
			calendar.MonthTD.innerHTML = ""
				+ calendar.MONTH[0][calendar.NewDATE.getMonth()]
				+ " – "
				+ calendar.NewDATE.getFullYear();

		// При отпускании мыши, убрать событие с окна:
			window.onmouseup = function()
				{
					window.onmousemove = null;
					window.onmouseup = null;

				// Если новая дата не равна отображённой, то перерисовать календарь:
					if (calendar.NewDATE.valueOf() != calendar.MonthTD.dateObj.valueOf())
						{
						// Установить дату с учётом месяца ячейки:
							calendar.date = calendar.getDateWithAddMonthToDate(calendar.date ,NewMONTH);
						// Пере-собрать календарь:
							calendar.print();
						}

				}
		};

// Или просто убрать год:
	window.onmouseup = function()
		{
			if (calendar.selectingMonth) {
				calendar.selectingMonth = true;
				return;
			}

			window.onmouseup = null;
			window.onmousemove = null;

		// Или просто убрать год:
			calendar.MonthTD.innerHTML = calendar.MONTH[0][calendar.MonthTD.month];

			event.target.click();
		}

};



earthlingCalendar.prototype.show = function ()
{
	var date = new Date();
	// На месяц вперёд:
	date.setMonth(date.getMonth() + 1);

	var calendar = this;
	if (this.constructor != earthlingCalendar)
		calendar = this.calendar;

	var div = calendar.containers.calendar;

	var value;
	if (/earthling-calendar-table-hidden/.test(div.className))
	{
		div.className = div.className.replace(/earthling-calendar-table-hidden/mig ,"");
		div.style.height = div.children[0].clientHeight + "px";
		value = "true";
	}
	else
	{
		div.className = (div.className ? div.className + " " : "") + ("earthling-calendar-table-hidden");
		div.style.height = 0;
		value = "false";
	}

	document.cookie="show-calendar=" + value + "; path=" + location.pathname + "; expire=" + date.toGMTString();
};



// Функция выбора всех дат месяца:
earthlingCalendar.prototype.selectMonthDays = function(event)
{
	event.preventDefault();

// Сбросить выбранную дату:
	this.DayTD = null;

	var calendar = this.calendar;

// Для отмены выделения:
	for(var d = 0; d < calendar.DAYs.length; d++)
	{
		if (calendar.selectingMonth) {
			calendar.selectingMonth = true;
			return;
		}

		if (calendar.DAYs[d].date == 1 && calendar.DAYs[d].month == this.month)
			calendar.DAYs[d].click();

	// Если первое найдено ... :
		if (calendar.DayTD)
			{
			// ... месяц уже другой ... :
				if (calendar.DAYs[d].month != this.month)
					calendar.DAYs[--d].click();

			// или и следующая дата последняя ... :
				if (d == calendar.DAYs.length - 1)
					calendar.DAYs[d].click();
			}
	}
};





// Функция выбора года:
earthlingCalendar.prototype.selectYear = function(event)
{
	if ( this.firstElementChild )
		return;

// Для IE:
	event = event || window.event;

// Координаты нажатия мыши:
	var start = event.clientY;

	var calendar = this.calendar;

	calendar.YearTD = this;

	window.onmousemove = function(event)
		{
			var diff = start - event.clientY;

			var NewYEAR = Math.round(diff / (window.innerHeight * 0.05));

		// Новая дата из основной для изменений:
			calendar.NewDATE = new Date( calendar.date.valueOf() );
		// Новая дата из основной для изменений:
			calendar.NewDATE.setFullYear( calendar.YearTD.year + NewYEAR );

		// Изменить год в ячейке:
			calendar.YearTD.innerHTML = calendar.NewDATE.getFullYear();

		// При отпускании мыши, убрать событие с окна:
			window.onmouseup = function()
				{
					window.onmousemove = null;
					window.onmouseup = null;

				// Если новая дата не равна отображённой, то перерисовать календарь:
					calendar.changeYear(calendar.NewDATE.getFullYear(), calendar.YearTD)
				}
		};

// Или просто убрать год:
	this.onmouseup = function()
		{
			window.onmouseup = null;
			window.onmousemove = null;

			this.click();
		}

};





// Функция выбора года:
earthlingCalendar.prototype.inputYear = function()
{
	var Input = this.firstElementChild;
	if ( !Input )
		{
			this.innerHTML = "";
			Input = document.createElement("input");
			Input.type = "text";
			Input.style.width = "5em";
			Input.style.height = "1em";
		// Год из ячейки:
			Input.value = this.year;
			this.appendChild(Input);
		}

// Установить курсор:
	Input.focus();
// Установить курсор в конце:
	Input.selectionStart = String(this.year).length - 1;
	Input.selectionEnd = String(this.year).length;

	Input.onblur = function()
		{
			this.parentNode.innerHTML = this.parentNode.year;
		};

	Input.onkeyup = function(event)
		{
		// Оставлять только цифры:
			this.value = this.value.replace(/[^\d]/g,"");

		// Если нажат ENTER, проверить и переписать календарь:
			if ( event.which == 13 )
				this.changeYear(this.value, this.parentNode);

		// Если нажат ESCAPE, убрать INPUT, вернуть год:
			if ( event.which == 27 )
				this.parentNode.innerHTML = this.parentNode.year;
		};

	Input.ondblclick = function()
		{
			this.select()
		}
};


earthlingCalendar.prototype.wordFormEventsCount = function (evensCOUNT ,wordsForms)
{
	var text = "";

	if (!wordsForms)
		wordsForms = this.eventsWordsForms;

	// Вывести сообщение о количестве запланированных мероприятий:
	if (evensCOUNT)
		text =
				+ evensCOUNT
				+ " "
				+ this.getWordForm(evensCOUNT ,wordsForms);

	// Если НЕТ мероприятий, то:
	else
		text = "нет " + wordsForms[2];

	return text;
};



// Функция выбора года:
earthlingCalendar.prototype.changeYear = function(NewYear ,YearTD)
{
	var yearDiff = this.date.getFullYear() - YearTD.year;
// Если новая дата не равна отображённой, то перерисовать календарь:
	if (NewYear != YearTD.year || NewYear != (this.date.getFullYear() + yearDiff))
		{
		// Новый год + разница с сегодняшним годом:
		// Установить:
			this.date.setFullYear(+NewYear + yearDiff);
		// Пере-собрать:
			this.print();
		}
};

earthlingCalendar.prototype.getWordForm = function(leng ,words)
{
	var last       = String(leng).substr(-1);
	var beforeLast = 0;
	if (leng > 9)
		beforeLast = String(leng).substr(-2 ,1);

	if (+beforeLast != 1 && +last == 1)
		return words[0];
	else if (+beforeLast != 1 && last != 0 && +last < 5)
		return words[1];
	else if (/\./.test(+leng))
		return words[1];
	else
		return words[2];
};

earthlingCalendar.prototype.printByHash = function ()
{
	if (this.settingInterval)
		return false;

	if (!/range=(\/?\d{4}-\d{1,2}-\d{1,2}){2}/.test(location.hash))
		return false;

	var dates = location.hash.match(/range=((?:\/?\d{4}-\d{1,2}-\d{1,2}){2})/)[1].split("/");
	var start = dates[0].split("-");
	var end   = dates[1].split("-");

	dates = this.firstAndSecond(new Date(start[0] ,start[1] - 1 ,start[2]) ,new Date(end[0] ,end[1] - 1 ,end[2]));

	start = dates[0];
	end   = dates[1];

	var matches = location.hash.match(/start=(\d{4}-\d{1,2}-\d{1,2})/);

	if (!matches || !matches[1])
		return false;

	var date = location.hash.match(/start=(\d{4}-\d{1,2}-\d{1,2})/)[1].split("-");
	date = new Date(date[0] ,date[1] - 1 ,date[2]);

	this.checkDate(date);

	this.setTimeByServerTimeDiff(date);

	this.date = date;
	this.print(
		 start.getDate() ,start.getMonth() ,start.getFullYear()
		,end.getDate() ,end.getMonth() ,end.getFullYear()
	);

	return true;
};

//earthlingCalendar.prototype.setEventsOnHashChange = function ()
//{
//	if ("onhashchange" in window)
//		document.body.onhashchange = function () {
//			if (this.settingHash)
//				return;
//			earthlingCalendar.prototype.printByHash();
//		};
//	else
//		object.addEventListener("hashchange", function () {
//			if (earthlingCalendar.prototype.settingHash)
//				return;
//			this.printByHash();
//		});
//};

//this.removeEventsOnHashChange = function ()
//{
//	document.body.onhashchange = null;
//	object.addEventListener("hashchange" ,null);
//};
