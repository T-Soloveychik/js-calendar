

var earthlingCalendar = function (elem ,options) {
	if (!document.contains(elem))
		throw new DOMException("Переданное значение не является элементом HTML!" ,elem);

	if (options && typeof options != "object")
		throw new DOMException("Переданные параметры не являются объектом!" ,options);

	if (!options.MONTH)
		this.MONTH = [
			[
				 "Январь"
				,"Февраль"
				,"Март"
				,"Апрель"
				,"Май"
				,"Июнь"
				,"Июль"
				,"Август"
				,"Сентябрь"
				,"Октябрь"
				,"Ноябрь"
				,"Декабрь"
			]
			,[
				 "января"
				,"февраля"
				,"марта"
				,"апреля"
				,"мая"
				,"июня"
				,"июля"
				,"августа"
				,"сентября"
				,"октября"
				,"ноября"
				,"декабря"
			]
		];

	this.PREV     = null;
	this.NEXT     = null;
	this.DAYs     = null;
	this.DayTD    = null;
	this.interval = null;
	this.selectingMonth = null;
	this.MonthTD = null;
	this.NewDATE = null;
	this.YearTD  = null;

	//Object.freeze(this.containers);
	//Object.seal(this);

	this.date = new Date();

	if (options.serverDate)
		this.serverDate = options.serverDate;

	if (!this.checkDate(this.serverDate))
		this.date = new Date(this.serverDate.valueOf());

	if (options.serverTimeZoneDiff)
		this.serverTimeZoneDiff = options.serverTimeZoneDiff;

	this.setTimeByServerTimeDiff(this.date);

	this.date.setHours(0);
	this.date.setMinutes(0);
	this.date.setSeconds(0);
	this.date.setMilliseconds(0);

	if (options.events)
		this.events = options.events;

	if (options.onIntervalSelected)
		this.onIntervalSelected = options.onIntervalSelected;

	if (options.eventsWordsForms)
		this.eventsWordsForms = options.eventsWordsForms;
	else
		this.eventsWordsForms = ["событие","события","событий"];

	this.container  = elem;
	this.containers = {};

	this.container.className = this.container.className + " earthling-calendar";

	var div = document.createElement("div");

	div.calendar = this;
	div.title = "Вернуться к сегодняшней дате";
	div.setAttribute("date-timestamp" ,this.timestamp);
	div.className = "earthling-calendar-to-today";
	div.innerHTML = "сегодня "
	                + this.date.getDate()
	                + " "
	                + this.MONTH[1][this.date.getMonth()]
	                + " "
	                + this.date.getFullYear()
	                + " года"
	                //+ " "
	                //+ this.date.getHours()
	                //+ ":"
	                //+ this.date.getMinutes()
	;

	this.containers.toToday = elem.appendChild(div);

	div                      = document.createElement("div");
	div.className            = "earthling-calendar-interval";
	this.containers.interval = elem.appendChild(div);

	if (options.startIntervalString)
		this.containers.interval.innerHTML = options.startIntervalString;

	div                         = document.createElement("div");
	div.className               = "earthling-calendar-events-count";
	this.containers.eventsCount = elem.appendChild(div);

	div                      = document.createElement("div");
	div.className            = "earthling-calendar-table";
	this.containers.calendar = elem.appendChild(div);

	if (options.startInterval)
	{
		this.interval = {
			 start       : this.setTimeByServerTimeDiff(new Date(options.startInterval[0]))
			,end         : this.setTimeByServerTimeDiff(new Date(options.startInterval[1]))
		};

		var date = this.interval.start;
		this.interval.startString = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate();

		date = this.interval.end;
		this.interval.endString = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate();
	}

	// Вернуться к сегодняшней дате:
	this.containers.toToday.onclick = this.onClickOnToday;


	this.containers.interval.calendar = this;
	this.containers.interval.onclick  = this.show;


	if (!this.printByHash())
	{
		this.print();

		if (!this.interval)
		{
			this.containers.today.click();

			var plusDays = 20;
			if (options.interval)
				plusDays = options.interval;

			plusDays = this.DAYs[this.DAYs.indexOf(this.containers.today) + +plusDays];

			plusDays.click();
		}
	}

	if (/show-calendar=false/.test(document.cookie))
		this.show();
};




earthlingCalendar.prototype.print = function(Day,Month,Year ,Day2,Month2,Year2)
	{
		var calendar = this;

		this.checkDate(this.date);

	// Массив для дат (создать/сбросить):
		this.DAYs = [];
	// Сбросить выбранную дату:
		this.DayTD = null;

		this.containers.calendar.innerHTML = null;

	// Начать с месяца ... с первого числа:
		var now = this.getDateWithAddMonthToDate(this.date, -1);
		now.setDate(1);

	// Для хранения значения выводимого месяца:
		var monthNow = null;

		var TABLE = document.createElement("TABLE");
	// Для отмены выделения:
		TABLE.onmousedown = function(){ return false; };
		this.containers.calendar.appendChild(TABLE);

	// Строка для отображения года/предыдущего/следующего года:
		var TR = document.createElement("TR");
		TABLE.appendChild(TR);


	// На месяц назад:
		this.PREV = document.createElement("TD");
		this.PREV.calendar  = this;
		this.PREV.setAttribute("rowspan",2);
		this.PREV.className = "earthling-calendar-to-month-previous";
		this.PREV.innerHTML = "&#9668;";
		this.PREV.onclick   = function() {
			calendar.date = calendar.getDateWithAddMonthToDate(calendar.date, -1);
			calendar.print();
		};
		TR.appendChild(this.PREV);

		// При нажатии стрелок менять месяц:
		window.onkeydown = function (event)
		{
			event = event || window.event;

			if (event.keyCode == 37)
				calendar.PREV.click();

			if (event.keyCode == 39)
				calendar.NEXT.click();
		};

	// Если меся 11 или более, то ... :
	// занять все три:
		var colspan = 3;
		if ( now.getMonth() >= 10 )
		// 11 объединить 2 ячейки, 12 - одну:
			colspan = (now.getMonth() == 10) ? 2 : 1;

		var year = document.createElement("TD");
		year.setAttribute("colspan",colspan);
		year.calendar    = this;
		year.className   = "earthling-calendar-year";
		year.innerHTML   = now.getFullYear();
		year.year        = now.getFullYear();
		year.onmousedown = this.selectYear;
		year.onclick     = this.inputYear;
		TR.appendChild(year);

	// Если месяц 11 или более:
		if ( now.getMonth() >= 10 )
			{
				colspan = (now.getMonth() == 10) ? 1 : 2;
				year    = document.createElement("TD");
				year.calendar    = this;
				year.setAttribute("colspan",colspan);
				year.className   = "earthling-calendar-year";
				year.innerHTML   = now.getFullYear() + 1;
			// Следующий год:
				year.year        = now.getFullYear() + 1;
				year.onmousedown = this.selectYear;
				year.onclick     = this.inputYear;
				TR.appendChild(year);
			}

	// На месяц вперёд:
		this.NEXT           = document.createElement("TD");
		this.NEXT.setAttribute("rowspan",2);
		this.NEXT.calendar  = this;
		this.NEXT.className = "earthling-calendar-to-month-next";
		this.NEXT.innerHTML = "&#9658;";
		this.NEXT.onclick   = function() {
			calendar.date = calendar.getDateWithAddMonthToDate(calendar.date, 1);
			calendar.print();
		};
		TR.appendChild(this.NEXT);

	// Строка для таблиц с месяцами:
		TR = document.createElement("TR");
		TABLE.appendChild(TR);


		var date;
		var WeekTR;
		var index;

		var tillMonth = this.getDateWithAddMonthToDate(this.date ,2);

		// Пока NOW не пройдёт по 3-м месяцам ... :
		while (now.getMonth() != tillMonth.getMonth())
			{
			// Если следующий месяц ... :
				if (monthNow != now.getMonth())
					{
						var TD = document.createElement("TD");
						TR.appendChild(TD);

						var MonthTABLE = document.createElement("TABLE");
						TD.appendChild(MonthTABLE);

						var MonthTR = document.createElement("TR");
						MonthTABLE.appendChild(MonthTR);

					// Название месяца:
						var MONTH = document.createElement("TD");
						MONTH.setAttribute("colspan",7);
						MONTH.calendar    = this;
						MONTH.innerHTML   = this.MONTH[0][now.getMonth()];
						MONTH.className   = "earthling-calendar-month";
						MONTH.dateObj     = new Date(now.valueOf());
						MONTH.year        = now.getFullYear();
						MONTH.month       = now.getMonth();
						MONTH.onmousedown = this.selectMonth;
						MONTH.onclick     = this.selectMonthDays;
						MonthTR.appendChild(MONTH);

						monthNow = now.getMonth();

						WeekTR = document.createElement("TR");
						WeekTR.month = now.getMonth();
						MonthTABLE.appendChild(WeekTR);

						/**
						 * Если первый день месяца не понедельник,
						 * то дойти до последнего понедельника предыдущего месяца
						 */
						while (now.getDay() != 1)
							{
								now.setDate(now.getDate() - 1);
							}

						/**
						 * Начать выводить с предыдущего месяца последнего понедельника
						 */
						while (now.getDate() != 1)
							{
								date = document.createElement("TD");
								date.calendar  = this;
								date.className = "earthling-calendar-previous-month";
								date.innerHTML = now.getDate();
								date.dateObj   = new Date(now.valueOf());
								date.timestamp = now.valueOf();
								date.year      = now.getFullYear();
								date.month     = now.getMonth();
								date.date      = now.getDate();
							// Перейти к месяцу:
								date.onclick = this.previousOrNextMonth;
								WeekTR.appendChild(date);

								now.setDate(now.getDate() + 1);
							}
					}

			// Если понедельник, то новая строка:
				if ( now.getDay() == 1 )
					{
						WeekTR = document.createElement("TR");
						WeekTR.month = now.getMonth();
						MonthTABLE.appendChild(WeekTR);
					}

			// Дата месяца:
				date = document.createElement("TD");
			// Даты в один массив:
				index = this.DAYs.length;
				this.DAYs[index] = date;
				date.calendar  = this;
				date.innerHTML = now.getDate();
				date.dateObj   = new Date(now.valueOf());
				date.index     = index;
				date.timestamp = now.valueOf();
				date.year      = now.getFullYear();
				date.month     = now.getMonth();
				date.date      = now.getDate();
				date.onclick   = this.daySelect;

				WeekTR.appendChild(date);

			// Если дата совпала с создаваемой датой:
				if (
						date.year  == this.serverDate.getFullYear()
					&&
						date.month == this.serverDate.getMonth()
					&&
						date.date  == this.serverDate.getDate()
					)
					{
						this.containers.today = date;
						date.className = "earthling-calendar-today";
						date.title     = "Сегодняшняя дата";
					}

			// Если дата совпала с создаваемой датой:
				if (
						calendar.interval
					&&
						date.dateObj.valueOf() >= calendar.interval.start.valueOf()
					&&
						date.dateObj.valueOf() <= calendar.interval.end.valueOf()
					)
						date.className = date.className + " " + calendar.selectedDayClassName;

				// Если есть дата в JSON-е мероприятий:
				if (
						this.events
					&&
						this.events[date.year]
					&&
						this.events[date.year][date.month + 1]
					&&
						this.events[date.year][date.month + 1][date.date]
					)
					{
						date.events = this.events[date.year][date.month + 1][date.date];
						date.innerHTML += "<SUP>" + (date.events.length) + "</SUP>";
						date.className = date.className + " earthling-calendar-date-event";
						date.title     = this.wordFormEventsCount(date.events.length);
					}


			// Если дата совпала с переданными месяцем и числом:
				if (Day == date.date && Month == date.month && Year == date.year)
					date.click();

			// Если дата совпала с переданными месяцем и числом:
				if (Day2 == date.date && Month2 == date.month && Year2 == date.year)
					date.click();

			// Прибавить один день:
				now.setDate(now.getDate() + 1);

			// Если новый месяц и первое число ... :
				if (monthNow != now.getMonth() && now.getDate() == 1)
					{
					// Если не Понедельник, Пройтись до Понедельника... :
						while (now.getDay() != 1)
							{
								date           = document.createElement("TD");
								date.calendar  = this;
								date.className = "earthling-calendar-next-month";
								date.innerHTML = now.getDate();
								date.dateObj   = new Date(now.valueOf());
								date.timestamp = now.valueOf();
								date.year      = now.getFullYear();
								date.month     = now.getMonth();
								date.date      = now.getDate();
							// Перейти к месяцу:
								date.onclick   = this.previousOrNextMonth;
								WeekTR.appendChild(date);

								now.setDate(now.getDate() + 1);
							}

					// Вернуться к ПЕРВОМУ:
						now.setDate(1);
					}
			}

		this.containers.calendar.style.height = this.containers.calendar.children[0].clientHeight + "px";
	};
